export const store = {
    state: {
        articles: []
    },
    getters: {
        getArticles: state => state.articles,
        getArticleById: state => id => state.articles.find(article => article.id == id)
    },
    mutations: {
        setArticles(state, articles) {
            state.articles = articles
        }
    },
    actions: {
        async loadArticles(ctx) {
            const articles = await fetch('/articles.json').then(r => r.json())
            if (articles) ctx.commit('setArticles', articles)
        }
    }
}