import { createApp } from 'vue'
import { createVuetify } from 'vuetify'
import { createStore } from 'vuex'
import App from './App.vue'
import router from './router'
import { store } from './store/store'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import 'vuetify/styles'

const app = createApp(App)
const vuetify = createVuetify({
    components,
    directives,
})

app.use(router).use(vuetify).use(createStore(store)).mount('#app')
