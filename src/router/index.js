import { createRouter, createWebHistory } from 'vue-router'
import Main from '../pages/Main.vue'
import About from '../pages/About.vue'
import News from '../pages/News.vue'
import Article from '../pages/Article.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'main',
      component: Main
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/news',
      name: 'news',
      component: News
    },
    {
      path: '/article/:id',
      name: 'article',
      component: Article
    }
  ]
})

export default router
